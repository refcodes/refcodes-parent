# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***This artifact provides the parent for all `REFCODES.ORG` toolkit's artifacts, defining the bill of material (BOM) version definitions of all third party libraries.***

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<artifactId>refcodes-parent</artifactId>
		<groupId>org.refcodes</groupId>
		<version>3.3.9</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-parent). Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-parent).

## Update versions ##

### Dependency updates ###

Show available dependency updates:

```
mvn versions:display-dependency-updates
```

### Plugin updates ###

Show available plugin updates:

```
mvn versions:display-plugin-updates
```

### Automatic updates (discouraged) ###

```
mvn versions:use-latest-releases
```

> It is very much discouraged to automatically update all versions!

## Plugin configuration ##

### org.graalvm.buildtools:native-maven-plugin###

Application specific native image's `JSON` configuration files are stored below the folder:

* `./src/main/resources/META-INF/native-image/<groupId>/<artifactId>` 

(`<groupId>` = e.g. project's Maven group ID or your applications' package namespace, `<artifactId>` = e.g. project's Maven artifact ID or your applications' name)

#### Dry-run ####

Dry-run your application to get the according native-image `JSON` configurations files below the `./target` folder:

```
java -agentlib:native-image-agent=config-output-dir=target -jar ./target/<jarFileName>
```

(`<jarFileName>` = your application's FatJAR file name)

#### Linux setup ####

1. Install GraalVM e.g. using [SDKMAN!](https://sdkman.io/)
2. `gu install native-image`
3. `sudo apt install build-essential`
4. `sudo apt install libz-dev`
5. `sudo apt libfreetype6-dev` (cannot find "-lfreetype" error

> Adapt the commands to the ones of your Linux distro!      

#### Windows setup ####                                           

1. Install [Visual Studio Community 20XY](https://visualstudio.microsoft.com/de/downloads), choose all C/C++ modules, English only!
2. Install [GraalVM](https://github.com/graalvm/graalvm-ce-builds)
3. Point `GRAALVM_HOME` environment variabel to GraalVM base foldet 
4. Add native image support `gu install native-image` to GraalVM

> Build in "x64 Native Tools Command Prompt" or "Developer Command Prompt for VS 2022" when using this pliugin to see C/C++ tools!

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/refcodes/refcodes-parent/issues)
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Issues ##

### Eclipse lifecycle warnings for maven enforcer plugin ###

In some Eclipse versions, tens of dozens of Maven `enforce` goal warnings may show up. this results in the "`maven-enforcer-plugin (goal "enforce") is ignored by m2e" warning by eclipse`" warnings message. To eliminate the warnings, go as follows (see [how-to-eliminate-the-maven-enforcer-plugin-goal-enforce-is-ignored-by-m2e-w](https://www.appsloveworld.com/eclipse/100/3/how-to-eliminate-the-maven-enforcer-plugin-goal-enforce-is-ignored-by-m2e-w)):

Edit the file "`.metadata/.plugins/org.eclipse.m2e.core/lifecycle-mapping-metadata.xml`" of your Eclipse workspace and add the below `<pluginExecution>`...`</pluginExecution>` snippet inside the file's `<pluginExecutions>`...`</pluginExecutions>` section:

```
<pluginExecutions>
	...
	<pluginExecution>
		<pluginExecutionFilter>
			<groupId>org.apache.maven.plugins</groupId>
			<artifactId>maven-enforcer-plugin</artifactId>
			<versionRange>[1.0.0,)</versionRange>
			<goals>
				<goal>enforce</goal>
			</goals>
		</pluginExecutionFilter>
		<action>
			<ignore />
		</action>
	</pluginExecution>
	...
</pluginExecutions>.
```

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.